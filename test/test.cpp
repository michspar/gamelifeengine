// test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "../gamelifeengine/Field.h";

int _tmain(int argc, _TCHAR* argv[])
{
	Field f;

	f.addCell( 0, 0 );
	f.addCell( 1, 0 );
	f.addCell( 2, 0 );
	f.nextDay();
	f.nextDay();
	f.nextDay();

	return 0;
}

