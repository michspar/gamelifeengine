#include "StdAfx.h"
#include "Cell.h"

Cell::Cell( long x, long y ) : m_x( x ), m_y( y )
{
}

Cell::~Cell()
{
}

bool Cell::operator==( const Cell & right ) const
{
	if ( m_x == right.m_x && m_y == right.m_y )
		return true;

	return false;
}

bool Cell::operator!=( const Cell & right ) const
{
	return !( *this == right );
}

bool Cell::operator==( const ICell * right ) const
{
	return *this == *dynamic_cast< const Cell * >( right );
}

bool Cell::operator!=( const ICell * right ) const
{
	return !( *this == right );
}

long Cell::getX() const
{
	return m_x;
}

long Cell::getY() const
{
	return m_y;
}