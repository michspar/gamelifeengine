#pragma once

struct ICell
{
	virtual bool operator==( const ICell * right ) const = 0;
	virtual bool operator!=( const ICell * right ) const = 0;

	virtual long getX() const = 0;
	virtual long getY() const = 0;
};