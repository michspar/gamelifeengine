#pragma once
#include "ICell.h"

class Cell : public ICell
{
	long m_x, m_y;

public:
	Cell( long x, long y );
	virtual ~Cell();

	virtual bool operator==( const ICell * right ) const;
	virtual bool operator!=( const ICell * right ) const;

	bool operator==( const Cell & right ) const;
	bool operator!=( const Cell & right ) const;

	virtual long getX() const;
	virtual long getY() const;
};