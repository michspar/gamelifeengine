#include "StdAfx.h"
#include "Field.h"
#include <vector>
#include "Cell.h"

using std::vector;

Field::Field(void)
{
}


Field::~Field(void)
{
}

void Field::addCell( long x, long y )
{
	if ( findCell( x, y ) != 0 )
		return;

	m_cells.push_back( Cell( x, y ) );
}

void Field::removeCell( long x, long y )
{
	Cell * cell = dynamic_cast< Cell * >( const_cast< ICell * >( findCell( x, y ) ) );

	if ( cell == 0 )
		return;

	m_cells.remove( *cell );
}

const ICell * Field::findCell( long x, long y ) const
{
	list< Cell >::const_iterator rval = std::find( m_cells.begin(), m_cells.end(), Cell( x, y ) );
	
	if ( rval == m_cells.end() )
		return 0;

	return &*rval;
}

int Field::getNeiboursCount( const Cell & cell ) const
{
	const long x = cell.getX(), y = cell.getY();
	int c = 0;

	for ( long iX = x - 1; iX < x + 2; iX++ ) 
		for ( long iY = y - 1; iY < y + 2; iY++ )
			if ( x != iX || y != iY )
				if ( findCell( iX, iY ) != 0 ) c++;

	return c;
}

vector< Field::Point > Field::getDeadNeibours( const Cell & cell ) const
{
	vector< Point > rval;
	const long x = cell.getX(), y = cell.getY();

	for ( long iX = x - 1; iX < x + 2; iX++ ) 
		for ( long iY = y - 1; iY < y + 2; iY++ )
			if ( x != iX || y != iY )
				if ( findCell( iX, iY ) == 0 )
				{
					Point p = { iX, iY };

					rval.push_back( p );
				}

	return rval;
}


void Field::removeDeadBodies()
{
	toRemove.clear();

	for ( list< Cell >::iterator i = m_cells.begin(); i != m_cells.end(); i++ )
	{
		int nCount = getNeiboursCount( *i );

		if ( nCount < 2 || nCount > 3 )
			toRemove.push_back( &*i );
	}
}

void Field::insertNewCells()
{
	toAdd.clear();

	for ( list< Cell >::iterator i = m_cells.begin(); i != m_cells.end(); i++ )
	{
		vector< Point > dNeibs = getDeadNeibours( *i );

		for ( vector< Point >::iterator j = dNeibs.begin(); j != dNeibs.end(); j++ )
		{
			Cell ghost( j->x, j->y );

			if ( getNeiboursCount( ghost ) == 3 && std::find( toAdd.begin(), toAdd.end(), ghost ) == toAdd.end() )
				toAdd.push_back( ghost );
		}
	}
}

void Field::commit()
{
	for ( vector< Cell * >::iterator i = toRemove.begin(); i != toRemove.end(); i++ )
		m_cells.remove( **i );

	for ( vector< Cell >::iterator i = toAdd.begin(); i != toAdd.end(); i++ )
		m_cells.push_back( *i );
}

void Field::nextDay()
{
	removeDeadBodies();
	insertNewCells();
	commit();
}