#pragma once
#include <list>
#include <vector>
#include "Cell.h"
#include "ICell.h"

using std::list;
using std::vector;

class Field
{
	struct Point
	{
		long x, y;
	};

	list< Cell > m_cells;
	vector< Cell * > toRemove;
	vector< Cell > toAdd;

	int getNeiboursCount( const Cell & cell ) const;
	vector< Point > getDeadNeibours( const Cell & cell ) const;
	void removeDeadBodies();
	void insertNewCells();
	void commit();

public:
	Field(void);
	virtual ~Field(void);

	void addCell( long x, long y );
	void removeCell( long x, long y );
	const ICell * findCell( long x, long y) const;
	void nextDay();
};

;